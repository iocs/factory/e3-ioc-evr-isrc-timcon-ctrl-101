require essioc
require mrfioc2, 2.3.1+15
require sdsioc, 0.0.1+1

#-All systems will have counters for 14Hz, PMorten and DoD
epicsEnvSet("EVREVTARGS" "N0=F14Hz,E0=14,N1=PMortem,E1=40,N2=DoD,E2=42")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=ISrc-TimCon:Ctrl-EVR-101,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=ISrc-TimCon:Ctrl-EVR-101"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=ISrc-TimCon:Ctrl-EVR-101,$(EVREVTARGS=)"

afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=ISrc-TimCon:Ctrl-EVR-101, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=ISrc-TimCon:Ctrl-EVR-101")')

dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-0-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-0-Evt-Trig0-SP, VAL=14"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-1-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-1-Evt-Trig0-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-2-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-2-Evt-Trig0-SP, VAL=12"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-3-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-3-Evt-Trig0-SP, VAL=13"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-4-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-4-Evt-Trig0-SP, VAL=125"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-5-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-5-Evt-Trig0-SP, VAL=40"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-8-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-8-Evt-Trig0-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-9-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-9-Evt-Trig0-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-10-Width-SP, VAL=0.8"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-10-Evt-Trig0-SP, VAL=6"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-10-Polarity-Sel, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-11-Width-SP, VAL=0.8"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-11-Evt-Trig0-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-11-Polarity-Sel, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-12-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-12-Evt-Trig0-SP, VAL=8"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-12-Evt-Trig1-SP, VAL=9"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back0-Src-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back1-Src-SP, VAL=1"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back2-Src-SP, VAL=49"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back3-Src-SP, VAL=4"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back4-Src-SP, VAL=5"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-Back6-Src-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP0-Src-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP1-Src-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP2-Src-SP, VAL=62"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP3-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FPUV0-Src-SP, VAL=52"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FPUV1-Src-SP, VAL=63"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FPUV2-Src-SP, VAL=52"
dbLoadRecords "initial-value.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FPUV3-Src-SP, VAL=63"

dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-0-Label-I, VAL=Magnetron Pulse"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-8-Label-I, VAL=Magnetron Start"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-9-Label-I, VAL=Magnetron End"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-10-Label-I, VAL=LEBT Chopper STOP"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:DlyGen-11-Label-I, VAL=LEBT Chopper START"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP0-Label-I, VAL=LEBT Chopper STOP"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP1-Label-I, VAL=LEBT Chopper START"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=ISrc-TimCon:Ctrl-EVR-101:Out-FP2-Label-I, VAL=LEBT Chopper Pulser Vcc"









#- Fixing IdCycle to 64bits
dbLoadRecords "cycleid-64bits.template"               "P=ISrc-TimCon:Ctrl-EVR-101:"
#- Fixing databuffer Information
dbLoadRecords "BDest.template" "P=ISrc-TimCon:Ctrl-EVR-101:,PV=BDest-I"
dbLoadRecords "BMod.template" "P=ISrc-TimCon:Ctrl-EVR-101:,PV=BMod-I"
dbLoadRecords "BPresent.template" "P=ISrc-TimCon:Ctrl-EVR-101:,PV=BPresent-I"
#- Configuring PVs to be archived
iocshLoad("archive-default.iocsh","P=ISrc-TimCon:Ctrl-EVR-101:")


#- ----------------------------------------------------------------------------
#- SDS Metadata Capture
#- ----------------------------------------------------------------------------
iocshLoad("$(sdsioc_DIR)/sdsCreateMetadataEVR.iocsh","PEVR=ISrc-TimCon:Ctrl-EVR-101:,F14Hz=F14Hz")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

